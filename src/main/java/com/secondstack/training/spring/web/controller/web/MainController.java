package com.secondstack.training.spring.web.controller.web;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 1:31 PM
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("/")
@SuppressWarnings("unchecked")
public class MainController {

    protected static Logger logger = Logger.getLogger("controller");

    @RequestMapping(method = RequestMethod.GET)
    public String home(ModelMap modelMap) {
        logger.debug("Received request to show home");
        return "home";
    }

    @RequestMapping(value = "spring", method = RequestMethod.GET)
    public String spring(ModelMap modelMap) {
        logger.debug("Received request to show spring");
        return "spring";
    }

    @RequestMapping(value = "jpa", method = RequestMethod.GET)
    public String jpa(ModelMap modelMap) {
        logger.debug("Received request to show jpa");
        return "jpa";
    }

    @RequestMapping(value = "extjs", method = RequestMethod.GET)
    public String extjs(ModelMap modelMap) {
        logger.debug("Received request to show extjs");
        return "extjs";
    }

    @RequestMapping(value = "hibernate", method = RequestMethod.GET)
    public String hibernate(ModelMap modelMap) {
        logger.debug("Received request to show hibernate");
        return "hibernate";
    }
}
