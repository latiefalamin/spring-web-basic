package com.secondstack.training.spring.web.controller.web;

import com.secondstack.training.spring.web.domain.Person;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("person")
public class PersonController {

    protected static Logger logger = Logger.getLogger("controller");

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(ModelMap modelMap) {
        logger.debug("Received request to get form person");
        Person person = new Person();
        person.setFirstName("Latif");
        person.setLastName("Al Amin");
        person.setAddress("Yogyakarta");
        modelMap.addAttribute("person", person);
        return "form";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@ModelAttribute("person") Person person, ModelMap modelMap) {
        logger.debug("Received request to create person");
        return "person";
    }

}
