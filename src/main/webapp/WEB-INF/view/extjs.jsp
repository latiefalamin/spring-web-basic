<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/resources/bootstrap/css/bootstrap.min.css">
    <script type="text/javascript" src="/resources/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/resources/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <style type="text/css">

            /* Customize the navbar links to be fill the entire space of the .navbar */
        .navbar .navbar-inner {
            padding: 0;
        }

        .navbar .nav {
            margin: 0;
            display: table;
            width: 100%;
        }

        .navbar .nav li {
            display: table-cell;
            width: 1%;
            float: none;
        }

        .navbar .nav li a {
            font-weight: bold;
            text-align: center;
            border-left: 1px solid rgba(255, 255, 255, .75);
            border-right: 1px solid rgba(0, 0, 0, .1);
        }

        .navbar .nav li:first-child a {
            border-left: 0;
            border-radius: 3px 0 0 3px;
        }

        .navbar .nav li:last-child a {
            border-right: 0;
            border-radius: 0 3px 3px 0;
        }
    </style>

    <title>2ndStack Spring Training</title>
</head>
<body>

<div class="container">

    <div class="masthead">
        <h3 class="muted">2ndStack Spring Framework Training</h3>

        <%--navbar--%>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <li><a href="<c:url value='/'/>">Home</a></li>
                        <li><a href="<c:url value='/spring'/>">Spring</a></li>
                        <li><a href="<c:url value='/jpa'/>">JPA</a></li>
                        <li class="active"><a href="#">Extjs</a></li>
                        <li><a href="<c:url value='/hibernate'/>">Hibernate</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--navbar-->

    </div>

    <!-- Main hero unit for a primary marketing message or call to action -->
    <div class="hero-unit">
        <h1>Sencha Ext JS </h1>

        <p>Sencha Ext JS is the leading standard for business-grade web application development. With over 100 examples, 1000 APIs, hundreds of components, a full documentation suite and built in themes, Ext JS provides the tools necessary to build robust desktop applications.</p>
    </div>

    <hr>

    <div class="footer">
        <p>&copy; 2ndStack 2013</p>
    </div>

</div>
<!-- /container -->
</body>
</html>